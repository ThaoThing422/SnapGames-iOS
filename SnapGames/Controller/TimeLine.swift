//
//  TimeLine.swift
//  SnapGames
//
//  Created by Jaime García Castán on 7/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit

class TimeLine: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda: CeldaFoto = tableView.dequeueReusableCell(withIdentifier: "CeldaFoto", for: indexPath) as! CeldaFoto
        celda.lblTituloImg?.text = "Titulo Imagen"
        celda.img?.image = #imageLiteral(resourceName: "kat.jpg")
        celda.mG?.image = #imageLiteral(resourceName: "kat.jpg")
        celda.coment?.image = #imageLiteral(resourceName: "kat.jpg")
        return celda
    }
    

}
