//
//  Queue.swift
//  SnapGames
//
//  Created by Jaime García Castán on 14/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit

class Queue: UIViewController, UITableViewDelegate, UITableViewDataSource, FirebaseAdminDelegate{
    
    @IBOutlet var table: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (DataHolder.getSharedInstance().queue?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda: CeldaCola = tableView.dequeueReusableCell(withIdentifier: "CeldaCola", for: indexPath) as! CeldaCola
        celda.img?.image = DataHolder.getSharedInstance().queue?[indexPath.row as Int].img
        celda.lblFechaH?.text = DataHolder.getSharedInstance().queue?[indexPath.row as Int].fecha
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "EditarTransition", sender: self)
    }
    
    func cargarQueue() {
        self.table?.reloadData()
    }

    
}
