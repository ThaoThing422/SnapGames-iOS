//
//  CeldaFoto.swift
//  SnapGames
//
//  Created by Jaime García Castán on 7/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit

class CeldaFoto: UITableViewCell {
    
    @IBOutlet var lblTituloImg: UILabel?
    @IBOutlet var img: UIImageView?
    @IBOutlet var mG: UIImageView?
    @IBOutlet var coment: UIImageView?
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
