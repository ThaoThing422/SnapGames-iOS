//
//  DataHolder.swift
//  SnapGames
//
//  Created by Jaime García Castán on 20/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit

class DataHolder: NSObject {
    private static let instance: DataHolder = DataHolder()
    var queue: Array<QueueItem>?
    var queueCount: Int?
    
    override init() {
        queue = Array<QueueItem>()
    }
    
    public static func getSharedInstance() -> DataHolder{
        return instance
    }
}
