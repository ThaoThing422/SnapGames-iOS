//
//  FirebaseAdmin.swift
//  SnapGames
//
//  Created by Jaime García Castán on 7/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit
import Firebase

class FirebaseAdmin: NSObject {
    private static let metodos: FirebaseAdmin = FirebaseAdmin()
    var databaseRef: DatabaseReference?
    var delegateLogin: FirebaseAdminDelegate?
    var uid: String?
    
    func configureFirebase() {
        FirebaseApp.configure()
    }
    
    public static func getSharedInstance() -> FirebaseAdmin{
        return metodos
    }
    
    func setDelegate(delegate: FirebaseAdminDelegate) {
        self.delegateLogin = delegate
    }
    
    func checkAccount() -> Bool {
        if(Auth.auth().currentUser?.uid == nil){
            return false
        }else{
            return true
        }
    }
    
    func iniciarSesion(email: String, password: String){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if(error == nil) {
                print("bieeen")
                self.uid = (user?.uid)!
                self.cargarQueue()
                self.delegateLogin?.dataLoaded!()
            }
            else {
                print("Fallooooo")
            }
        }
    }
    
    func cerrarSesion() {
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func cargarQueue() {
        Database.database().reference().child("queue/" + uid!).observe(DataEventType.value, with: { (snapshot) in
            let data: Array<AnyObject> = (snapshot.value as? Array<AnyObject>)!
            DataHolder.getSharedInstance().queue?.removeAll()
            DataHolder.getSharedInstance().queueCount = data.count
            for item in data{
                let qi: QueueItem = QueueItem(imgName: item["img"] as! String, fecha: item["fecha"] as! String)
                DataHolder.getSharedInstance().queue?.append(qi)
                self.descargarImg(qi: qi)
            }
        })
    }
    
    func descargarImg(qi: QueueItem) {
        Storage.storage().reference().child("queue/" + qi.imgName!).getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print(error)
            } else {
                qi.img? = UIImage(data: data!)!
                DataHolder.getSharedInstance().queueCount = (DataHolder.getSharedInstance().queueCount)! - 1
                if(DataHolder.getSharedInstance().queueCount == 0){
                    print("imagenes descargadas")
                    
                }
            }
        }
    }

}

@objc protocol FirebaseAdminDelegate{
    @objc optional func dataLoaded()
    @objc optional func cargarQueue()
}

